package com.example.app_movies.view;

import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.app_movies.adapter.MovieAdapter;
import com.example.app_movies.model.Movie;
import com.example.app_movies.viewModel.MovieViewModel;
import com.example.app_movies.R;

import java.util.List;

public class MainActivity extends AppCompatActivity {

    private MovieViewModel movieViewModel;
    boolean isLoading = false;
    RecyclerView movieContainerView;
    MovieAdapter adapter;

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initRecyclerView();
        initAndSetAdapter();

        populateData();
        //initScrollListener();

        onClick(findViewById(R.id.poster));
    }

    private void initRecyclerView() {
        movieContainerView = findViewById(R.id.movie_container);
        movieContainerView.setLayoutManager(new GridLayoutManager(this, 2));
        movieContainerView.setHasFixedSize(true);
    }

    private void initAndSetAdapter() {
        adapter = new MovieAdapter();
        movieContainerView.setAdapter(adapter);
    }

    private void populateData() {
        movieViewModel = ViewModelProviders.of(this).get(MovieViewModel.class);
        movieViewModel.allMovies().observe(this, adapter::setMovies);
    }

    public void onClick(View v) {
        System.out.println("OLA");
    }

    /**
     * if the bottom-most item is visible we show the loading view and populate the next list
     */
    private void initScrollListener() {
        movieContainerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            List<Movie> movieList = movieViewModel.allMovies().getValue();
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
                if (!isLoading) {
                    if (linearLayoutManager != null && linearLayoutManager.findLastCompletelyVisibleItemPosition() == movieList.size() - 1) {
                        //bottom of list!
                        loadMore(movieList);
                        isLoading = true;
                    }
                }
            }
        });
    }

    private void loadMore(List<Movie> movieList) {
        movieList.add(null);
        adapter.notifyItemInserted(movieList.size() - 1);

        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                movieList.remove(movieList.size() - 1);
                int scrollPosition = movieList.size();
                adapter.notifyItemRemoved(scrollPosition);
                int currentSize = scrollPosition;
                int nextLimit = currentSize + 4;

                while (currentSize - 1 < nextLimit) {
                    //adicionar mais um movie. mas como é que assinalo que era suposto n apresentar todos a partir da lista fetched pelo serviço?
                    movieList.add(new Movie("Movie" + currentSize, "abc"));
                    currentSize++;
                }

                adapter.notifyDataSetChanged();
                isLoading = false;
            }
        }, 2000);
    }

}
