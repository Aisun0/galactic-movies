package com.example.app_movies.api.utils;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Helper class for reusing retrofit implementations using the "Gson-converter"
 */
public class RetrofitGsonApiImplementer<T> {

    private String url;

    private Class<T> classType;

    public RetrofitGsonApiImplementer(String url, Class<T> classType){
        this.url = url;
        this.classType = classType;
    }

    public T create() {
        return new Retrofit.Builder()
                .baseUrl(url)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
                .create(classType);
    }

}
