package com.example.app_movies.viewModel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.example.app_movies.data.MovieRepository;
import com.example.app_movies.model.Movie;

import java.util.List;

public class MovieViewModel extends AndroidViewModel {

    private MovieRepository repository = MovieRepository.getInstance();
    private LiveData<List<Movie>> movies;

    public MovieViewModel(@NonNull Application application) {
        super(application);
        movies = repository.fetchMovies();
    }

    /**
     * Exposes movies contained in this {@link AndroidViewModel}
     * @return all contained movies
     */
    public LiveData<List<Movie>> allMovies() {
        return movies;
    }

}
