package com.example.app_movies.model;

import com.google.gson.annotations.SerializedName;

public class Movie {

    private String title;

    @SerializedName("poster_path")
    private String poster;

    public Movie(String title, String poster) {
        this.title = title;
        this.poster = poster;
    }

    public String getTitle() {
        return title;
    }

    public String getPoster() {
        return poster;
    }

}
