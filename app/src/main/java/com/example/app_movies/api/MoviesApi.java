package com.example.app_movies.api;

import com.example.app_movies.model.MovieDBApiResponse;

import retrofit2.Call;
import retrofit2.http.GET;

public interface MoviesApi {

    @GET("movie/top_rated?api_key=5cefcc5e0cbccd10d1ced2c1a2dc8992&language=en-US&page=1")
    Call<MovieDBApiResponse> getTopRatedMovies();

}
