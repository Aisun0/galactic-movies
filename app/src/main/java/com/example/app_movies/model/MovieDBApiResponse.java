package com.example.app_movies.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class MovieDBApiResponse {

    @SerializedName("results")
    private List<Movie> movies;

    public List<Movie> getMovies() {
        return movies;
    }

}
