package com.example.app_movies.data;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.example.app_movies.api.MoviesApi;
import com.example.app_movies.api.utils.RetrofitGsonApiImplementer;
import com.example.app_movies.model.Movie;
import com.example.app_movies.model.MovieDBApiResponse;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

//todo make Repository<T> with constructor taking data source and accepting retrofit api implementations
public class MovieRepository {

    private static final String BASE_URL = "https://api.themoviedb.org/3/";
    private static MovieRepository instance;

    private MovieRepository() {
        // singleton instance must be acquired through @code{getInstance()} invocation
    }

    // todo move this to appropriate place, part of the code can be reused
    private MoviesApi moviesApi = new RetrofitGsonApiImplementer<>(BASE_URL, MoviesApi.class).create();

    /*
     * Fetches movies from the provided data source
     * */
    public LiveData<List<Movie>> fetchMovies() {
        MutableLiveData<List<Movie>> movies = new MutableLiveData<>();

        moviesApi.getTopRatedMovies().enqueue(new Callback<MovieDBApiResponse>() {
            @Override
            public void onResponse(Call<MovieDBApiResponse> call, Response<MovieDBApiResponse> response) {
                if (!response.isSuccessful()) {
                    try {
                        throw new Exception("Server unreachable : Error " + response.code());
                    } catch (Exception e) {
                        e.printStackTrace();
                        return;
                    }
                }
                if (response.body() != null) movies.setValue(response.body().getMovies());
                else movies.setValue(new ArrayList<>());
            }

            @Override
            public void onFailure(Call<MovieDBApiResponse> call, Throwable t) {
                //todo handle failure
                try {
                    throw t;
                } catch (Throwable throwable) {
                    throwable.printStackTrace();
                    System.exit(255);
                }
            }
        });
        return movies;
    }

    public static MovieRepository getInstance() {
        if (instance == null) instance = new MovieRepository();
        return instance;
    }

}
